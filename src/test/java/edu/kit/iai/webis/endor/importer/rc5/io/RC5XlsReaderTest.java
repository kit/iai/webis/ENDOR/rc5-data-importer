package edu.kit.iai.webis.endor.importer.rc5.io;

import java.io.InputStream;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import edu.kit.iai.webis.endor.importer.rc5.io.RC5XlsReader;

/**
 * Test {@link RC5XlsReader}
 */
class RC5XlsReaderTest {

	/**
	 * Test the read method with reference data and classpath test xls (<code>/rc5_test.xls</code>)
	 * 
	 * @param referenceData
	 *            reference data for example xls file
	 * @throws Exception
	 *             on errors
	 */
	@ParameterizedTest
	@MethodSource("referenceData")
	void testReadWithExampleData(final Map<ZonedDateTime, Float> referenceData) throws Exception {
		final RC5XlsReader reader = new RC5XlsReader(ZoneId.of("UTC+1")); // CET
		try (InputStream exampleStream = RC5XlsReader.class.getResourceAsStream("/rc5_test.xls")) {
			final Map<ZonedDateTime, Float> readData = reader.read(exampleStream);
			Assertions.assertEquals(referenceData, readData, "Reference data differs from read data");
		}
	}

	/**
	 * Create reference data for example xls file, values already (like data that was read) in UTC
	 * 
	 * @return reference data for example xls file
	 */
	static Stream<Map<ZonedDateTime, Float>> referenceData() {
		final Map<ZonedDateTime, Float> data = new LinkedHashMap<>();
		data.put(ZonedDateTime.of(2017, 12, 22, 8, 2, 25, 0, ZoneOffset.UTC), 26f);
		data.put(ZonedDateTime.of(2017, 12, 22, 8, 3, 25, 0, ZoneOffset.UTC), 26f);
		data.put(ZonedDateTime.of(2017, 12, 22, 8, 4, 25, 0, ZoneOffset.UTC), 25.5f);
		data.put(ZonedDateTime.of(2017, 12, 22, 8, 5, 25, 0, ZoneOffset.UTC), 25.1f);
		return Stream.of(data);
	}

}