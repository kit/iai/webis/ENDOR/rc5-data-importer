package edu.kit.iai.webis.endor.importer.rc5;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import edu.kit.iai.webis.endor.importer.rc5.bean.SensorInfoBean;
import edu.kit.iai.webis.endor.importer.rc5.config.InfluxDBConfig;
import edu.kit.iai.webis.endor.importer.rc5.config.RC5Config;
import edu.kit.iai.webis.endor.importer.rc5.io.InfluxDBWriter;
import edu.kit.iai.webis.endor.importer.rc5.io.RC5XlsReader;

/**
 * Importer application for RC5 XLS data
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude = DataSourceAutoConfiguration.class)
public class RC5XlsDataImporter implements CommandLineRunner {

	/**
	 * Logger for import application
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(RC5XlsDataImporter.class);

	private final RC5Config rc5Conf;
	private final InfluxDBConfig influxConf;
	private final SensorInfoBean infoBean;

	@Autowired
	public RC5XlsDataImporter(final RC5Config rc5Conf, final InfluxDBConfig influxConf, final SensorInfoBean infoBean) throws Exception {
		super();
		this.rc5Conf = rc5Conf;
		this.influxConf = influxConf;
		this.infoBean = infoBean;

		rc5Conf.validate(); // throws exception on errors
		influxConf.validate(); // throws exception on errors
	}

	@Override
	public void run(final String... args) throws Exception {
		try (InfluxDBWriter writer = new InfluxDBWriter(this.influxConf)) {
			final RC5XlsReader reader = new RC5XlsReader(this.rc5Conf.getTimeZoneId());

			final Path xlsPath = Paths.get(this.rc5Conf.getXls());
			final Optional<Map<String, String>> sensorTags = this.infoBean.getSensorInfo(this.rc5Conf.getId());

			RC5XlsDataImporter.LOGGER.info("Reading data from XLS file at: {}", xlsPath);
			final Map<ZonedDateTime, Float> data = reader.read(xlsPath);

			RC5XlsDataImporter.LOGGER.info("Read {} values, writing to InfluxDB {}", data.size(), this.influxConf.getUrl());
			writer.write(data, sensorTags);
		} catch (final Exception e) {
			RC5XlsDataImporter.LOGGER.error("Error during data import", e);
		}
	}

	/**
	 * Application entry point
	 * 
	 * @param args
	 *            CLI arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(RC5XlsDataImporter.class, args);
	}

}
