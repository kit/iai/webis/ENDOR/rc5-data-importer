package edu.kit.iai.webis.endor.importer.rc5.config;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * InfluxDB configuration
 */
@ConfigurationProperties(prefix = "influx")
@Component
public class InfluxDBConfig {

	/**
	 * URL to InfluxDB
	 */
	private String url;

	/**
	 * User to access InfluxDB with
	 */
	private String user;

	/**
	 * Password to access InfluxDB with
	 */
	private String pass;

	/**
	 * Database name to use
	 */
	private String dbName;

	/**
	 * Measurement name to use
	 */
	private String measurement;

	/**
	 * Get the URL to InfluxDB
	 * 
	 * @return URL to InfluxDB
	 */
	public String getUrl() {
		return this.url;
	}

	/**
	 * Set the URL to InfluxDB
	 * 
	 * @param url
	 *            the URL to InfluxDB to set
	 */
	public void setUrl(final String url) {
		this.url = url;
	}

	/**
	 * Get the user to access InfluxDB with
	 * 
	 * @return user to access InfluxDB with
	 */
	public String getUser() {
		return this.user;
	}

	/**
	 * Set the user to access InfluxDB with
	 * 
	 * @param user
	 *            the user to access InfluxDB with to set
	 */
	public void setUser(final String user) {
		this.user = user;
	}

	/**
	 * Get the password to access InfluxDB with
	 * 
	 * @return password to access InfluxDB with
	 */
	public String getPass() {
		return this.pass;
	}

	/**
	 * Set the password to access InfluxDB with
	 * 
	 * @param pass
	 *            the password to access InfluxDB with to set
	 */
	public void setPass(final String pass) {
		this.pass = pass;
	}

	/**
	 * Get the database name to use
	 * 
	 * @return database name to use
	 */
	public String getDbName() {
		return this.dbName;
	}

	/**
	 * Set the database name to use
	 * 
	 * @param dbName
	 *            the database name to use to set
	 */
	public void setDbName(final String dbName) {
		this.dbName = dbName;
	}

	/**
	 * Get the measurement name to use
	 * 
	 * @return measurement name to use
	 */
	public String getMeasurement() {
		return this.measurement;
	}

	/**
	 * Set the measurement name to use
	 * 
	 * @param dbName
	 *            the measurement name to use to set
	 */
	public void setMeasurement(final String measurement) {
		this.measurement = measurement;
	}

	/**
	 * <p>
	 * Determine if this config has all config parameters to access InfluxDB
	 * </p>
	 * <p>
	 * Note that <b>this includes a non-empty password</b>, using no password is no option nowadays!
	 * </p>
	 * 
	 * @return <code>true</code> if InfluxDB information available, <code>false</code> otherwise
	 */
	public boolean isValid() {
		return StringUtils.isNoneBlank(this.url, this.user, this.pass, this.dbName, this.measurement); // setting empty passwords is no option nowadays!
	}

	/**
	 * Validate all fields and throw exception with information about missing fields if required
	 * 
	 * @throws IllegalStateException
	 *             if required fields are missing
	 */
	public void validate() throws IllegalStateException {
		if (!this.isValid()) {
			final List<String> missingFields = new LinkedList<>();

			final BiConsumer<String, String> fieldMsg = (name, val) -> {
				if (StringUtils.isBlank(val)) {
					missingFields.add(val);
				}
			};

			fieldMsg.accept("url", this.url);
			fieldMsg.accept("user", this.user);
			fieldMsg.accept("pass", this.pass);
			fieldMsg.accept("dbName", this.dbName);
			fieldMsg.accept("measurement", this.measurement);

			throw new IllegalStateException("Missing influx configuration fields: " + StringUtils.join(fieldMsg, ", "));
		}
	}

}