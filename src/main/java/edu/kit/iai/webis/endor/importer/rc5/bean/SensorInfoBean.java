package edu.kit.iai.webis.endor.importer.rc5.bean;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import edu.kit.iai.webis.endor.importer.rc5.config.RC5Config;

/**
 * Bean with additional information about sensor
 */
@Component
public class SensorInfoBean {

	/**
	 * Sensor info map
	 */
	private final Map<String, Map<String, String>> sensorInfo;

	/**
	 * Create sensor meta data bean
	 * 
	 * @param rc5Config
	 *            configuration to get info file path from
	 * @param gson
	 *            instance to use for unmarshalling
	 */
	@Autowired
	public SensorInfoBean(final RC5Config rc5Config, final Gson gson) {
		this.sensorInfo = rc5Config.getSensorInfoFile().map(m -> this.loadMetaDataJson(m, gson)).orElse(Collections.emptyMap());
	}

	/**
	 * Get information available for sensor
	 * 
	 * @param sensorId
	 *            ID of the sensor
	 * @return meta data map for sensor if available
	 */
	public Optional<Map<String, String>> getSensorInfo(final String sensorId) {
		return Optional.ofNullable(this.sensorInfo.get(sensorId));
	}

	/**
	 * Load information from JSON file
	 * 
	 * @param infoJsonFilePath
	 *            path to JSON file
	 * @param gson
	 *            GSON instance to use for unmarshalling
	 * @return loaded map
	 * @throws IllegalArgumentException
	 *             when something goes wrong (wraps possible I/O exceptions)
	 */
	private Map<String, Map<String, String>> loadMetaDataJson(final String infoJsonFilePath, final Gson gson) {
		try (BufferedReader reader = Files.newBufferedReader(Paths.get(infoJsonFilePath))) {
			final Type mapType = new TypeToken<Map<String, Map<String, String>>>() {
			}.getType();
			return gson.fromJson(reader, mapType);
		} catch (final IOException e) {
			// convert to runtime exception for optional friendly behavior
			throw new IllegalArgumentException("Bad info JSON file provided", e);
		}
	}
}
