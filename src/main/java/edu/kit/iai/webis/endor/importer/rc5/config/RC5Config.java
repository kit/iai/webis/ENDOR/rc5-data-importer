package edu.kit.iai.webis.endor.importer.rc5.config;

import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiConsumer;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Configuration for source RC5 data
 */
@ConfigurationProperties(prefix = "rc5")
@Component
public class RC5Config {

	/**
	 * Path to XLS file with RC5 data
	 */
	private String xls;

	/**
	 * ID of the sensor
	 */
	private String id;

	/**
	 * Time zone of the sensor data
	 */
	private String timeZone;

	/**
	 * Sensor info file
	 */
	private Optional<String> sensorInfoFile;

	/**
	 * Get the path to XLS file with RC5 data
	 * 
	 * @return path to XLS file with RC5 data
	 */
	public String getXls() {
		return this.xls;
	}

	/**
	 * @param xls
	 *            the xls to set
	 */
	public void setXls(final String xls) {
		this.xls = xls;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * @return the sensorInfoFile
	 */
	public Optional<String> getSensorInfoFile() {
		return this.sensorInfoFile;
	}

	/**
	 * @param sensorInfoFile
	 *            the sensorInfoFile to set
	 */
	public void setSensorInfoFile(final Optional<String> sensorInfoFile) {
		this.sensorInfoFile = sensorInfoFile;
	}

	/**
	 * Get the time zone info
	 * 
	 * @return time zone info
	 */
	public String getTimeZone() {
		return this.timeZone;
	}

	/**
	 * Set time zone info
	 * 
	 * @param timeZone
	 *            time zone info to set
	 */
	public void setTimeZone(final String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * Get the time zone Id
	 * 
	 * @return time zone Id
	 */
	public ZoneId getTimeZoneId() {
		return ZoneId.of(this.timeZone);
	}

	/**
	 * Determine if this config has all config parameters to read RC5 files
	 * 
	 * @return <code>true</code> if all required config parameters available, <code>false</code> otherwise
	 */
	public boolean isValid() {
		return StringUtils.isNoneBlank(this.xls, this.id, this.timeZone);
	}

	/**
	 * Validate all fields and throw exception with information about missing fields if required
	 * 
	 * @throws IllegalStateException
	 *             if required fields are missing
	 */
	public void validate() throws IllegalStateException {
		if (!this.isValid()) {
			final List<String> missingFields = new LinkedList<>();

			final BiConsumer<String, String> fieldMsg = (name, val) -> {
				if (StringUtils.isBlank(val)) {
					missingFields.add(val);
				}
			};

			fieldMsg.accept("xls", this.xls);
			fieldMsg.accept("id", this.id);
			fieldMsg.accept("timeZone", this.timeZone);

			throw new IllegalStateException("Missing RC5 configuration fields: " + StringUtils.join(fieldMsg, ", "));
		}
	}

}