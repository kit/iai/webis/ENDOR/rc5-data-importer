package edu.kit.iai.webis.endor.importer.rc5.io;

import java.time.ZonedDateTime;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import org.influxdb.BatchOptions;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.influxdb.dto.Point.Builder;

import edu.kit.iai.webis.endor.importer.rc5.config.InfluxDBConfig;

import org.influxdb.dto.Query;

/**
 * InfluxDB writer
 */
public class InfluxDBWriter implements AutoCloseable {

	/**
	 * Influx database to work with
	 */
	private final InfluxDB influxDB;

	/**
	 * Measurement to write to
	 */
	private final String measurement;

	/**
	 * Create new writer instance
	 * 
	 * @param conf
	 *            InfluxDB configuration
	 * @throws Exception
	 *             on errors
	 */
	public InfluxDBWriter(final InfluxDBConfig conf) throws Exception {
		this.influxDB = InfluxDBFactory.connect(conf.getUrl(), conf.getUser(), conf.getPass());

		this.influxDB.query(new Query("CREATE DATABASE " + conf.getDbName()));
		this.influxDB.setDatabase(conf.getDbName());
		this.influxDB.enableBatch(BatchOptions.DEFAULTS);

		this.measurement = conf.getMeasurement();
	}

	/**
	 * Close wrapped InfluxDB instance
	 */
	@Override
	public void close() throws Exception {
		this.influxDB.close();
	}

	/**
	 * Write data with no additional tags
	 * 
	 * @param values
	 *            values to write
	 */
	public void write(final Map<ZonedDateTime, Float> values) {
		this.write(values, Optional.empty());
	}

	/**
	 * Write data with additional tags
	 * 
	 * @param values
	 *            values to write
	 * @param additionalTags
	 *            additional tags for each point
	 */
	public void write(final Map<ZonedDateTime, Float> values, final Optional<Map<String, String>> additionalTags) {
		values.forEach((ts, val) -> {
			// create builder
			final Builder builder = Point.measurement(this.measurement).time(ts.toInstant().toEpochMilli(), TimeUnit.MILLISECONDS).addField("value", val);

			// add meta data as tags
			additionalTags.ifPresent(builder::tag);

			// and write value to influx
			this.influxDB.write(builder.build());
		});

	}

}
