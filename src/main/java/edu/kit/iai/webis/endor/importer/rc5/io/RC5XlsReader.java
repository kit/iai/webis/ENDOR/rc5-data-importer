package edu.kit.iai.webis.endor.importer.rc5.io;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Reader for the RC5 XLS format
 */
public class RC5XlsReader {

	/**
	 * Logger to use
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(RC5XlsReader.class);

	/**
	 * Date formatter
	 */
	private final DateTimeFormatter dateFormatter;

	/**
	 * Create reader for RC5 XLS data file
	 * 
	 * @param timezone
	 *            timezone to interpret read data
	 */
	public RC5XlsReader(final ZoneId timezone) {
		super();
		this.dateFormatter = DateTimeFormatter.ofPattern("yy/MM/dd HH:mm:ss").withZone(timezone);
	}

	/**
	 * Read data from XLS file of RC5 sensor
	 * 
	 * @param xlsPath
	 *            path to XLS file to read data from
	 * @return loaded data <b>with time stamps already converted to UTC</b>
	 */
	public Map<ZonedDateTime, Float> read(final Path xlsPath) throws IOException {
		if (xlsPath == null) {
			throw new IllegalArgumentException("Path to file must be provided");
		}

		RC5XlsReader.LOGGER.info("Reading RC5 XLS data from {}", xlsPath.toAbsolutePath());

		return this.read(Files.newInputStream(xlsPath));
	}

	/**
	 * Read data from XLS of RC5 sensor via input stream
	 * 
	 * @param stream
	 *            stream with XLS data to read from
	 * @return loaded data <b>with time stamps already converted to UTC</b>
	 */
	public Map<ZonedDateTime, Float> read(final InputStream stream) throws IOException {
		final Map<ZonedDateTime, Float> data = new LinkedHashMap<>();
		try (InputStream xlsInStream = stream) {
			final Workbook xls = WorkbookFactory.create(xlsInStream);

			// RC5 XLS export has data in sheet at index 0
			final Sheet sheet = xls.getSheetAt(0);

			final int firstDataRowNum = this.findFirstDataRowNum(sheet);
			final int lastDataRowNum = sheet.getLastRowNum();

			for (int rowNum = firstDataRowNum; rowNum < lastDataRowNum; rowNum++) {
				final Row dataRow = sheet.getRow(rowNum);
				if (dataRow != null) {
					final Cell cellTime = dataRow.getCell(1); // always column B
					final Cell cellVal = dataRow.getCell(2); // always column C

					if (cellTime != null && cellTime.getCellType() == CellType.STRING && cellVal != null && cellVal.getCellType() == CellType.STRING) {
						final String timeStamp = cellTime.getStringCellValue();

						final ZonedDateTime zonedTime = ZonedDateTime.parse(timeStamp, this.dateFormatter).withZoneSameInstant(ZoneOffset.UTC);
						final float value = Float.parseFloat(cellVal.getStringCellValue());

						data.put(zonedTime, value);
					}
				}
			}
		} catch (final IOException e) {
			RC5XlsReader.LOGGER.error("Error while reading RC5 XLS data", e);
			throw e;
		}
		return data;
	}

	/**
	 * Find first row with actual data. Searches for string 'S/N' in column A which is always in the row before first data
	 * 
	 * @param s
	 *            sheet to use
	 * @return number of first row with data
	 */
	private int findFirstDataRowNum(final Sheet s) {
		for (int rowNum = s.getFirstRowNum(); rowNum < s.getLastRowNum(); rowNum++) {
			final Row row = s.getRow(rowNum);
			if (row != null) {
				final Cell cellA = row.getCell(0);
				if (cellA != null && cellA.getCellType() == CellType.STRING) {
					if (StringUtils.equals("S/N", StringUtils.trim(cellA.getStringCellValue()))) {
						return rowNum + 1;
					}
				}
			}
		}
		return -1;
	}
}
