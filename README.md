# ENDOR (ENergy Data of Offices and Residential buildings) Project

## Data importer for RC-5 mobile sensors XLS format

This software reads the Excel XLS export of the RC-5 USB sensors and writes the data directly to InfluxDB.

Copyright (c) 2018-2020 Karlsruhe Institute of Technology (KIT), Institute for Automation and Applied Informatics (IAI)

For license information see LICENSE.txt

### Usage

#### Configuration Parameter
The software requires various configuration parameters like information which input XLS to read and how to access the backend database.
All parameters may be provided in any way supported by [Spring Configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-external-config).

All configuration parameters are mandatory unless specified otherwise.

- For the RC5 sensor input data:
  - `rc5.xls`: Path to XLS file to read
  - `rc5.id`: ID of the sensor
  - `rc5.timeZone`: Time zone definition compatible with [java.time.ZoneId](https://docs.oracle.com/javase/8/docs/api/java/time/ZoneId.html#of-java.lang.String-) (e.g. `UTC+1`)
  - `rc5.sensorInfoFile` *(optional)*: JSON file with additional information added as tag to each measurement point


- For the InfluxDB access:
  - `influx.url`: URL to InfluxDB with protocol, e.g. `http://localhost:8086`
  - `influx.user`: User to access InfluxDB with
  - `influx.pass`: Password to access InfluxDB with. **Please note:** Access to InfluxDB without full credentials (including a non-empty password) is **not supported**.
  - `influx.dbName`: Database name in InfluxDB to use (will be created if not present).
  - `influx.measurement`: Measurement to use


#### Example YML configuration file

The configuration for Spring applications could be provided via an external YML configuration file.
For this application it would look like this:

```yml
rc5:
  xls: 'SensorExport1.xls'
  id: 'SENSOR_ID_1'
  timeZone: 'UTC+1'
  sensorInfoFile: 'example-sensor-information.json'

influx:
  url: http://localhost:8086
  user: myuser
  pass: mysupersecretpassword
  dbName: mydatabase
  measurement: temperature
```

#### Example invocation
With the following command the config YML file described above would be used as configuration input for the RC5 data importer software:

```bash
java -jar rc5-importer.jar --spring.config.location=file:./endor-rc5-config.yml
```

#### Optional Sensor Info Files Format

```json
{
  "SENSOR_ID_1": {
    "tag1": "MyVal A",
    "tag2": "MyVal B",
    "tag3": "MyVal C"
  },
  "SENSOR_ID_2": {
    "tag1": "MyVal AA",
    "tag2": "MyVal BB",
    "tag4": "MyVal D"
  }
}
```

If the sensor ID (value of configuration parameter `rc5.id`) is `SENSOR_ID_1`, the tags of each measurement point are being supplemented with `tag1=MyVal A`, `tag2=MyVal B`, `tag2=MyVal C`
